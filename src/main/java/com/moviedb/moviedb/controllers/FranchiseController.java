package com.moviedb.moviedb.controllers;

import com.moviedb.moviedb.exception.ResourceNotFoundException;
import com.moviedb.moviedb.models.domain.Franchise;
import com.moviedb.moviedb.models.dtos.CharacterDto;
import com.moviedb.moviedb.models.dtos.FranchiseDto;
import com.moviedb.moviedb.models.dtos.MovieDto;
import com.moviedb.moviedb.models.dtos.MovieIDsDto;
import com.moviedb.moviedb.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins="*")

// Franchise controller class - FULL CRUD for franchise table

public class FranchiseController {

    //Injections
    @Autowired
    private FranchiseService franchiseService;

    // GET REQUESTS
    @GetMapping(value = "/franchise")
    public ResponseEntity<List<FranchiseDto>> getAllFranchise() {
        return franchiseService.getAllFranchise();
    }

    @GetMapping(value = "/franchise/{id}")
    public ResponseEntity<FranchiseDto> getFranchiseById(@PathVariable Long id) {
       return franchiseService.getFranchiseById(id);
    }

    @GetMapping(value = "/franchise/{id}/movies")
    public ResponseEntity<List<MovieDto>> getAllMoviesInFranchise(@PathVariable Long id) {
        return franchiseService.getAllMoviesInFranchise(id);
    }

    @GetMapping(value = "/franchise/{id}/characters")
    public ResponseEntity<List<CharacterDto>> getAllCharactersInFranchise(@PathVariable Long id) {
        return franchiseService.getAllCharactersInFranchise(id);
    }

    // PUT REQUESTS
    @PutMapping("/franchise/{id}")
    public ResponseEntity<FranchiseDto> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise){
       return franchiseService.updateFranchise(id, franchise);
    }

    // Updating movies in a franchise
    @PutMapping("/franchise/{id}/movies")
    public ResponseEntity<List<MovieDto>> updateMoviesInFranchise(
            @PathVariable Long id,
            @RequestBody MovieIDsDto movieIds) {

        return franchiseService.updateMoviesInFranchise(id, movieIds);
    }

    // POST REQUESTS
    @PostMapping("/franchise")
    public FranchiseDto createFranchise(@RequestBody Franchise franchise) {
        return franchiseService.createFranchise(franchise);
    }

    // DELETE REQUESTS
    @DeleteMapping("/franchise/{id}")
    public HashMap<String, Boolean> deleteFranchise(@PathVariable(value = "id") Long franchiseId)
            throws ResourceNotFoundException {
        return franchiseService.deleteFranchise(franchiseId);
    }

}
