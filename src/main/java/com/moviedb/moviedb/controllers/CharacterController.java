package com.moviedb.moviedb.controllers;

import com.moviedb.moviedb.models.dtos.CharacterDto;
import com.moviedb.moviedb.exception.ResourceNotFoundException;
import com.moviedb.moviedb.models.domain.Character;
import com.moviedb.moviedb.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins="*")

// Character controller class - FULL CRUD for character table

public class CharacterController {

    //Injections
    @Autowired
    CharacterService characterService;

    //GET REQUESTS
    @GetMapping(value = "/characters")
    public ResponseEntity<List<CharacterDto>> getAllCharacter() {
        return characterService.getAllCharacter();
    }


    @GetMapping(value = "/characters/{id}")
    public ResponseEntity<CharacterDto> getCharacterById(@PathVariable Long id) {
        return characterService.getCharacterById(id);
    }

    //PUT REQUESTS
    @PutMapping("/characters/{id}")
    public ResponseEntity<CharacterDto> updateCharacter(@PathVariable Long id, @RequestBody Character character){
       return characterService.updateCharacter(id,character);
    }

    //POST REQUESTS
    @PostMapping("/characters")
    public CharacterDto createCharacter(@RequestBody Character character) {
        return characterService.createCharacter(character);
    }

    //DELETE REQUESTS
    @DeleteMapping("/characters/{id}")
    public HashMap<String, Boolean> deleteCharacter(@PathVariable(value = "id") Long characterId)
            throws ResourceNotFoundException {

        return characterService.deleteCharacter(characterId);
    }

}
