package com.moviedb.moviedb.controllers;

import com.moviedb.moviedb.models.dtos.CharacterDto;
import com.moviedb.moviedb.models.dtos.CharactersIDsDto;
import com.moviedb.moviedb.models.dtos.MovieDto;
import com.moviedb.moviedb.exception.ResourceNotFoundException;
import com.moviedb.moviedb.models.domain.Movie;
import com.moviedb.moviedb.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;


@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins="*")

// Movie controller class - FULL CRUD for movie table

public class MovieController {

    // Injections
    @Autowired
    private MovieService movieService;

    // GET REQUESTS
    @GetMapping(value = "/movies")
    public ResponseEntity<List<MovieDto>> getAllMovies() {
        return movieService.getAllMovies();
    }

    @GetMapping(value = "/movies/{id}")
    public ResponseEntity<MovieDto> getMoviesById(@PathVariable Long id) {
        return movieService.getMoviesById(id);
    }

    @GetMapping(value = "/movies/{id}/characters")
    public ResponseEntity<List<CharacterDto>> getAllCharactersInMovie(@PathVariable Long id) {
        return movieService.getAllCharactersInMovie(id);
    }

    // PUT REQUESTS
    @PutMapping("/movies/{id}")
    public ResponseEntity<MovieDto> updateMovie(@PathVariable Long id, @RequestBody Movie movie){
        return movieService.updateMovie(id, movie);
    }

    // Updating characters in a movie
    @PutMapping("/movies/{id}/characters")
    public ResponseEntity<List<CharacterDto>> updateCharactersInMovie(
            @PathVariable Long id,
            @RequestBody CharactersIDsDto characterIds) {
        return movieService.updateCharactersInMovie(id, characterIds);
    }

    // POST REQUESTS
    @PostMapping("/movies")
    public MovieDto createMovie(@RequestBody Movie movie) {
        return movieService.createMovie(movie);
    }

    // DELETE REQUESTS
    @DeleteMapping("/movies/{id}")
    public HashMap<String, Boolean> deleteMovie(@PathVariable(value = "id") Long movieId)
            throws ResourceNotFoundException {
        return movieService.deleteMovie(movieId);
    }

}
