package com.moviedb.moviedb.repositories;

import com.moviedb.moviedb.models.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// Repository for MovieController
@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
}
