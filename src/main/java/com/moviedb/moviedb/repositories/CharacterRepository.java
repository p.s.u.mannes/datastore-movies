package com.moviedb.moviedb.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.moviedb.moviedb.models.domain.Character;

// Repository for CharacterController
@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
}
