package com.moviedb.moviedb.repositories;

import com.moviedb.moviedb.models.domain.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// Repository for FranchiseController
@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
