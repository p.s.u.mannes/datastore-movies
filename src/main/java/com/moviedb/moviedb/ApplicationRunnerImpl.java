package com.moviedb.moviedb;

import com.moviedb.moviedb.models.domain.Character;
import com.moviedb.moviedb.models.domain.Franchise;
import com.moviedb.moviedb.models.domain.Movie;
import com.moviedb.moviedb.repositories.CharacterRepository;
import com.moviedb.moviedb.repositories.FranchiseRepository;
import com.moviedb.moviedb.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

// Used to generate some initial data in the database
// Runs at startup

@Component
public class ApplicationRunnerImpl implements ApplicationRunner {

    // INJECTIONS
    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private FranchiseRepository franchiseRepository;

    // RUNS AT STARTUP
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("ApplicationRunner starting...");

        Franchise amazon = new Franchise("Amazon", "A franchise");
        Franchise netflix = new Franchise("Netflix", "Another franchise");

        franchiseRepository.save(amazon);
        franchiseRepository.save(netflix);

        Character emmaStone = new Character("Emma Stone", "Stone", "Female", "https://www.imdb.com/title/tt0464339/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        Character christianBale = new Character("Christian Bale", "Bale", "Male", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");

        characterRepository.save(emmaStone);
        characterRepository.save(christianBale);

        Movie batman = new Movie("Batman", "Action", 1985, "Tarantino",
                "https://www.movie1.picture",
                "https://www.movie1.trailer");
        Movie lalaland = new Movie("Lalaland", "Musical", 1999, "Spielberg",
                "https://www.movie2.picture",
                "https://www.movie2.trailer");
        Movie pulpFiction = new Movie("Pulp Fiction", "Classic", 1984, "Tarantino",
                "https://www.movie3.picture",
                "https://www.movie3.trailer");

        batman.setFranchise(amazon);
        batman.setCharacters(List.of(christianBale));

        lalaland.setFranchise(netflix);
        lalaland.setCharacters(List.of(emmaStone));

        pulpFiction.setFranchise(netflix);
        pulpFiction.setCharacters(List.of(christianBale, emmaStone));

        movieRepository.save(batman);
        movieRepository.save(lalaland);
        movieRepository.save(pulpFiction);

        System.out.println("ApplicationRunner stopping...\n" +
                "Starting main application...");
    }
}
