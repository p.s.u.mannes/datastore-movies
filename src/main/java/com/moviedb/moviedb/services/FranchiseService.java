package com.moviedb.moviedb.services;

import com.moviedb.moviedb.exception.ResourceNotFoundException;
import com.moviedb.moviedb.models.domain.Character;
import com.moviedb.moviedb.models.domain.Franchise;
import com.moviedb.moviedb.models.domain.Movie;
import com.moviedb.moviedb.models.dtos.CharacterDto;
import com.moviedb.moviedb.models.dtos.FranchiseDto;
import com.moviedb.moviedb.models.dtos.MovieDto;
import com.moviedb.moviedb.models.dtos.MovieIDsDto;
import com.moviedb.moviedb.repositories.FranchiseRepository;
import com.moviedb.moviedb.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

// Service for FranchiseController - contains all CRUD logic
@Service
public class FranchiseService {

    // Injecting repositories with autowired
    // to automatically generate the constructor
    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieRepository movieRepository;

    // Get all franchises
    public ResponseEntity<List<FranchiseDto>> getAllFranchise() {
        List<FranchiseDto> dtoList = new ArrayList<>();
        List<Franchise> franchises = franchiseRepository.findAll();

        // Loop through every franchise of response and add to list of DTO
        for (Franchise franchise: franchises) {
            dtoList.add(mapFranchiseModelToFranchiseDto(franchise));
        }

        // Return OK response
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(dtoList, resp);
    }

    // Get franchise by id
    public ResponseEntity<FranchiseDto> getFranchiseById(Long id) {
        Optional<Franchise> franchiseOptional = franchiseRepository.findById(id);

        // If optional exists, create DTO and return it
        if (franchiseOptional.isPresent()) {
            Franchise franchiseModel = franchiseOptional.get();
            FranchiseDto franchiseDto = mapFranchiseModelToFranchiseDto(franchiseModel);
            return ResponseEntity.ok(franchiseDto);
        }

        // Return not found otherwise
        return ResponseEntity.notFound().build();
    }

    // Get all movies in a franchise
    public ResponseEntity<List<MovieDto>> getAllMoviesInFranchise(Long id) {
        Optional<Franchise> franchiseOptional = franchiseRepository.findById(id);

        // If optional exists, create DTO and return it as follows:
        if (franchiseOptional.isPresent()) {
            Franchise franchiseModel = franchiseOptional.get();
            // Get all the movies
            List<Movie> movies = franchiseModel.getMovies();
            List<MovieDto> dtoList = new ArrayList<>();

            // Loop through every movie and add to list of DTO
            for (Movie movie: movies) {
                dtoList.add(mapMovieModelToMovieDto(movie));
            }

            // Reponse is ok
            HttpStatus resp = HttpStatus.OK;
            return new ResponseEntity<>(dtoList, resp);
        }

        // Return not found response otherwise
        return ResponseEntity.notFound().build();
    }

    // Get all characters in franchise
    public ResponseEntity<List<CharacterDto>> getAllCharactersInFranchise(Long id) {
        Optional<Franchise> franchiseOptional = franchiseRepository.findById(id);
        if (franchiseOptional.isPresent()) {
            Franchise franchiseModel = franchiseOptional.get();

            // Create list of movies and initialize DTO list return type
            List<Movie> movies = franchiseModel.getMovies();
            List<CharacterDto> dtoList = new ArrayList<>();

            // Loop through every movie, convert to DTO and add to return DTO list
            for (Movie movie: movies) {
                List<Character> characters = movie.getCharacters();
                for (Character character: characters) {
                    dtoList.add(mapCharacterModelToCharacterDto(character));
                }

            }

            // Generate OK response
            HttpStatus resp = HttpStatus.OK;
            return new ResponseEntity<>(dtoList, resp);
        }

        // No result found response otherwise
        return ResponseEntity.notFound().build();
    }

    // Update a franchise
    public ResponseEntity<FranchiseDto> updateFranchise(Long id, Franchise franchise){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        /*
         This is to ensure some level of security, making sure someone
         hasn't manipulated our request body or a client error.
        */
        if(!id.equals(franchise.getId())){
            return ResponseEntity.badRequest().build();
        }
        returnFranchise = franchiseRepository.save(franchise);

        // Map the object to the DTO to return a DTO
        FranchiseDto franchiseDto = mapFranchiseModelToFranchiseDto(returnFranchise);

        // Return OK response
        status = HttpStatus.OK;
        return new ResponseEntity<>(franchiseDto, status);
    }

    // Create a franchise, returns a DTO
    public FranchiseDto createFranchise(Franchise franchise) {
        franchiseRepository.save(franchise);
        return mapFranchiseModelToFranchiseDto(franchise);
    }

    // Delete a franchise
    public HashMap<String, Boolean> deleteFranchise(Long franchiseId)
            throws ResourceNotFoundException {
        Franchise franchise = franchiseRepository.findById(franchiseId)
                // Throw custom error if not found
                .orElseThrow(() -> new ResourceNotFoundException("Franchise not found for this id :: " + franchiseId));

        franchiseRepository.delete(franchise);
        HashMap<String, Boolean> response = new HashMap<>();

        // Return "deleted" and true
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    // Update movies in a franchise
    public ResponseEntity<List<MovieDto>> updateMoviesInFranchise(
            Long id,
            MovieIDsDto movieIds) {

        Franchise franchise = franchiseRepository.getById(id);

        // Using .stream() and .map() to iterate through every movie id
        // and to create a movie list
        List<Movie> returnMovies = movieIds.getMovieIds()
                .stream()
                .map(cId -> movieRepository
                        .findById(cId)
                        .get()).toList();

        List<MovieDto> dtoList = new ArrayList<>();

        // Loop through movies and add them to DTO
        // Save the movies in hibernate
        for (Movie movie: returnMovies) {
            movie.setFranchise(franchise);
            dtoList.add(mapMovieModelToMovieDto(movie));
            movieRepository.save(movie);
        }

        // Finally add all movies to the franchise
        franchise.getMovies().addAll(returnMovies);
        franchiseRepository.save(franchise);

        // Return OK status
        HttpStatus status;
        status = HttpStatus.OK;
        return new ResponseEntity<>(dtoList, status);
    }

    /**
     * Map the Franchise model to the franchise dto.
     * @param model Franchise model.
     * @return FranchiseDto.
     */
    private FranchiseDto mapFranchiseModelToFranchiseDto(Franchise model) {
        FranchiseDto dto = new FranchiseDto();
        dto.setId(model.getId().toString());
        dto.setDescription(model.getDescription());
        dto.setName(model.getName());
        return dto;
    }

    /**
     * Map the Movie model to the movie dto.
     * @param model movie model.
     * @return MovieDto.
     */
    private MovieDto mapMovieModelToMovieDto(Movie model) {
        MovieDto dto = new MovieDto();
        dto.setTitle(model.getTitle());
        dto.setDirector(model.getDirector());
        dto.setGenre(model.getGenre());
        dto.setPicture(model.getPicture());
        dto.setId(String.valueOf(model.getId()));
        dto.setTrailer(model.getTrailer());
        dto.setYear(String.valueOf(model.getYear()));
        return dto;
    }

    /**
     * Map the Character model to the Character dto.
     * @param model Character model.
     * @return CharacterDto.
     */
    private CharacterDto mapCharacterModelToCharacterDto(Character model) {
        CharacterDto dto = new CharacterDto();
        dto.setId(model.getId().toString());
        dto.setCharacterName(model.getCharacterName());
        dto.setAlias(model.getAlias());
        dto.setGender(model.getGender());
        dto.setPicture(model.getPicture());
        return dto;
    }
}
