package com.moviedb.moviedb.services;

import com.moviedb.moviedb.exception.ResourceNotFoundException;
import com.moviedb.moviedb.models.domain.Character;
import com.moviedb.moviedb.models.domain.Movie;
import com.moviedb.moviedb.models.dtos.CharacterDto;
import com.moviedb.moviedb.models.dtos.CharactersIDsDto;
import com.moviedb.moviedb.models.dtos.MovieDto;
import com.moviedb.moviedb.repositories.CharacterRepository;
import com.moviedb.moviedb.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

// Service for MovieController - contains all CRUD logic
@Service
public class MovieService {

    // Injecting repositories with autowired
    // to automatically generate the constructor
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CharacterRepository characterRepository;

    // Get all movies
    public ResponseEntity<List<MovieDto>> getAllMovies() {
        List<MovieDto> dtoList = new ArrayList<>();
        List<Movie> movies = movieRepository.findAll();

        // Loop through every movie of response and add to list of DTO
        for (Movie movie: movies) {
            dtoList.add(mapMovieModelToMovieDto(movie));
        }

        // Return OK response
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(dtoList, resp);
    }

    public ResponseEntity<MovieDto> getMoviesById(Long id) {
        // The database returns me the model (Movie)
        Optional<Movie> movieOptional = movieRepository.findById(id);
        if (movieOptional.isPresent()) {
            Movie movieModel = movieOptional.get();
            // Mapping model to movieDto to avoid infinite loop with properties (Movie -> Franchise -> Movies -> Franchise -> etc).
            MovieDto movieDto = mapMovieModelToMovieDto(movieModel);
            // The controller will return the movieDto (MovieDTO)
            return ResponseEntity.ok(movieDto);
        }
        return ResponseEntity.notFound().build();
    }

    // Get all characters in a Movie
    public ResponseEntity<List<CharacterDto>> getAllCharactersInMovie(Long id) {
        Optional<Movie> movieOptional = movieRepository.findById(id);

        // If movie was found (optional is present)
        if (movieOptional.isPresent()) {
            Movie movieModel = movieOptional.get();
            List<Character> characters = movieModel.getCharacters();
            List<CharacterDto> dtoList = new ArrayList<>();

            // Loop through every character in the Character list,
            // convert to DTO, and add to DTO list return type
            for (Character character: characters) {
                dtoList.add(mapCharacterModelToCharacterDto(character));
            }

            // Return OK and the DTO list
            HttpStatus resp = HttpStatus.OK;
            return new ResponseEntity<>(dtoList, resp);
        }

        return ResponseEntity.notFound().build();
    }

    // Update the movie
    public ResponseEntity<MovieDto> updateMovie(Long id, Movie movie){
        Movie returnMovie = movieRepository.getById(id);
        MovieDto movieDto = mapMovieModelToMovieDto(returnMovie);

        HttpStatus status;
        /*
         This is to ensure some level of security, making sure someone
         hasn't manipulated our request body or a client error.
        */
        if(!id.equals(movie.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(movieDto,status);
        }

        // Save the movie in hibernate and
        // generate the DTO return type
        returnMovie = movieRepository.save(movie);
        movieDto = new MovieDto(returnMovie);

        // Return ok
        status = HttpStatus.OK;
        return new ResponseEntity<>(movieDto, status);
    }

    // Create a movie, returns DTO
    public MovieDto createMovie(Movie movie) {
        movieRepository.save(movie);
        return mapMovieModelToMovieDto(movie);
    }

    // Delete a movie
    public HashMap<String, Boolean> deleteMovie(Long movieId)
            throws ResourceNotFoundException {
        Movie movie = movieRepository.findById(movieId)
                // Throw custom error if not found
                .orElseThrow(() -> new ResourceNotFoundException("Movie not found for this id :: " + movieId));

        movieRepository.delete(movie);
        HashMap<String, Boolean> response = new HashMap<>();

        // Response is "deleted" and TRUE
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    // Update characters in a movie
    public ResponseEntity<List<CharacterDto>> updateCharactersInMovie(
            Long id,
            CharactersIDsDto characterIds) {

        // Get the movie by ID
        Movie movie = movieRepository.getById(id);

        // Generate list of characters
        // Get the character ID's
        List<Character> returnCharacters = characterIds.getCharacterIds()
                // Convert every ID into Character object
                .stream().map(cId -> characterRepository
                        .findById(cId)
                        // Store in list
                        .get()).toList();

        // Initialize return type
        List<CharacterDto> dtoList = new ArrayList<>();

        // Loop through Character list and store in return DTO list
        for (Character character: returnCharacters) {
            dtoList.add(mapCharacterModelToCharacterDto(character));
        }

        // Add all characters to the movie and save it
        movie.getCharacters().addAll(returnCharacters);
        movieRepository.save(movie);

        // Return OK
        HttpStatus status;
        status = HttpStatus.OK;
        return new ResponseEntity<>(dtoList, status);
    }


    /**
     * Map the Movie model to the movie dto.
     * @param model movie model.
     * @return MovieDto.
     */
    private MovieDto mapMovieModelToMovieDto(Movie model) {
        MovieDto dto = new MovieDto();
        dto.setTitle(model.getTitle());
        dto.setDirector(model.getDirector());
        dto.setGenre(model.getGenre());
        dto.setPicture(model.getPicture());
        dto.setId(String.valueOf(model.getId()));
        dto.setTrailer(model.getTrailer());
        dto.setYear(String.valueOf(model.getYear()));
        return dto;
    }

    /**
     * Map the Character model to the Character dto.
     * @param model Character model.
     * @return CharacterDto.
     */
    private CharacterDto mapCharacterModelToCharacterDto(Character model) {
        CharacterDto dto = new CharacterDto();
        dto.setId(model.getId().toString());
        dto.setCharacterName(model.getCharacterName());
        dto.setAlias(model.getAlias());
        dto.setGender(model.getGender());
        dto.setPicture(model.getPicture());
        return dto;
    }

}
