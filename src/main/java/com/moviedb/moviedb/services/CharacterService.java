package com.moviedb.moviedb.services;

import com.moviedb.moviedb.exception.ResourceNotFoundException;
import com.moviedb.moviedb.models.domain.Character;
import com.moviedb.moviedb.models.dtos.CharacterDto;
import com.moviedb.moviedb.repositories.CharacterRepository;
import com.moviedb.moviedb.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

// Service for CharacterController - contains all CRUD logic
@Service
public class CharacterService {

    // Injecting repositories with autowired
    // to automatically generate the constructor
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharacterRepository characterRepository;

    // Get all characters
    public ResponseEntity<List<CharacterDto>> getAllCharacter() {
        List<Character> characters = characterRepository.findAll();
        List<CharacterDto> dtoList = new ArrayList<>();

        // Loop through every character of response and add to list of DTO
        for (Character character: characters) {
            dtoList.add(mapCharacterModelToCharacterDto(character));
        }

        // Return OK response
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(dtoList, resp);
    }

    // Get every character by id
    public ResponseEntity<CharacterDto> getCharacterById(Long id) {
        Optional<Character> characterOptional = characterRepository.findById(id);

        // Check if conditional object is present
        if (characterOptional.isPresent()) {
            Character characterModel = characterOptional.get();
            // Map the result to DTO
            CharacterDto characterDto = mapCharacterModelToCharacterDto(characterModel);
            return ResponseEntity.ok(characterDto);
        }

        //Return not found if not found
        return ResponseEntity.notFound().build();
    }

    // Update a character
    public ResponseEntity<CharacterDto> updateCharacter(Long id, Character character){
        Character returnCharacter = new Character();
        HttpStatus status;

        /*
         This is to ensure some level of security, making sure someone
         hasn't manipulated our request body or a client error.
        */
        if(!id.equals(character.getId())){
            return ResponseEntity.badRequest().build();
        }
        returnCharacter = characterRepository.save(character);

        CharacterDto characterDto = mapCharacterModelToCharacterDto(returnCharacter);

        // Return response OK
        status = HttpStatus.OK;
        return new ResponseEntity<>(characterDto, status);
    }

    // Create a character
    public CharacterDto createCharacter(Character character) {
        characterRepository.save(character);
        // Map response body to DTO object
        return mapCharacterModelToCharacterDto(character);
    }

    // Delete a character
    public HashMap<String, Boolean> deleteCharacter(Long characterId)
            throws ResourceNotFoundException {
        Character character = characterRepository.findById(characterId)
                // Throw custom error if not found
                .orElseThrow(() -> new ResourceNotFoundException("Character not found for this id :: " + characterId));

        // delete the character
        characterRepository.delete(character);
        HashMap<String, Boolean> response = new HashMap<>();

        // Create response HashMap -> Return "deleted" and TRUE
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    /**
     * Map the Character model to the Character dto.
     * @param model Character model.
     * @return CharacterDto.
     */
    private CharacterDto mapCharacterModelToCharacterDto(Character model) {
        CharacterDto dto = new CharacterDto();
        dto.setId(model.getId().toString());
        dto.setCharacterName(model.getCharacterName());
        dto.setAlias(model.getAlias());
        dto.setGender(model.getGender());
        dto.setPicture(model.getPicture());
        return dto;
    }
}
