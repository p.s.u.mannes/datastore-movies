package com.moviedb.moviedb.models.domain;

import javax.persistence.*;
import java.util.List;

// Franchise entity
@Entity
@Table(name = "franchise")
public class Franchise {

    // Id auto-increments
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "name")
    String name;

    @Column(name = "description")
    String description;

    // Mapping one franchise to many movies
    @OneToMany(mappedBy = "franchise")
    private List<Movie> movies;

    // Constructor
    public Franchise(String name, String description) {
        this.name = name;
        this.description = description;
    }

    // Default constructor
    public Franchise() {

    }

    // GETTERS AND SETTERS

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
