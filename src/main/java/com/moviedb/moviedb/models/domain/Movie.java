package com.moviedb.moviedb.models.domain;

import javax.persistence.*;
import java.util.List;

// Movie entity
@Entity
@Table(name = "movie")
public class Movie {

    // Id auto-increments
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "genre")
    private String genre;

    @Column(name = "year")
    private int year;

    @Column(name = "director")
    private String director;

    @Column(name = "movie_picture")
    private String picture;

    @Column(name = "trailer")
    private String trailer;

    // Mapping all the movie id's to one franchise
    @ManyToOne
    @JoinColumn(name = "franchise_id", referencedColumnName = "id")
    private Franchise franchise;

    // Mapping all the movie id's to different character id's
    @ManyToMany
    @JoinTable(
            name = "character_movie",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "character_id")
    )
    private List<Character> characters;

    //Constructor
    public Movie(String title,
                 String genre, int year,
                 String director, String picture,
                 String trailer) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.director = director;
        this.picture = picture;
        this.trailer = trailer;
    }

    //Default constructor
    public Movie() {

    }

    // GETTERS AND SETTERS

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }
}
