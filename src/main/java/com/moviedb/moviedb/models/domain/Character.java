package com.moviedb.moviedb.models.domain;

import javax.persistence.*;
import java.util.List;

// Character entity
@Entity
@Table(name = "character")
public class Character {

    // Id auto-increments
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "character_name")
    private String characterName;

    @Column(name = "alias")
    private String alias;

    @Column(name = "gender")
    private String gender;

    @Column(name = "picture")
    private String picture;

    // Mapping characters to different movies
    @ManyToMany(mappedBy = "characters")
    private List<Movie> movies;

    // Constructor
    public Character(String characterName, String alias, String gender, String picture) {
        this.characterName = characterName;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
    }

    // Default constructor
    public Character() {

    }

    // GETTERS AND SETTERS

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
