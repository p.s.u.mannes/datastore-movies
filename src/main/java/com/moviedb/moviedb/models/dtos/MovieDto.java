package com.moviedb.moviedb.models.dtos;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.moviedb.moviedb.models.domain.Movie;

// DTO for Movie class to encapsulate the information for the controller
public class MovieDto {

    private String id;
    private String title;
    private String genre;
    private String year;
    private String director;
    private String picture;
    private String trailer;

    // CONSTRUCTORS
    public MovieDto(Movie movie) {
        this.id = movie.getId().toString();
        this.title = movie.getTitle();
        this.genre = movie.getGenre();
        this.year = String.valueOf(movie.getYear());
        this.director = movie.getDirector();
        this.picture = movie.getPicture();
        this.trailer = movie.getTrailer();
    }

    public MovieDto() {

    }

    // GETTERS AND SETTERS WITH @JsonGetter annotation
    // to parse the variables into the JSON format
    @JsonGetter("id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonGetter("title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonGetter("genre")
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @JsonGetter("year")
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @JsonGetter("director")
    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @JsonGetter("picture")
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @JsonGetter("trailer")
    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }
}
