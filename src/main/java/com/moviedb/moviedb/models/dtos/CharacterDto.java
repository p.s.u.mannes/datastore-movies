package com.moviedb.moviedb.models.dtos;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.moviedb.moviedb.models.domain.Character;

// DTO for Character class to encapsulate the information for the controller
public class CharacterDto {

    private String id;
    private String name;
    private String alias;
    private String gender;
    private String picture;

    // CONSTRUCTORS
    public CharacterDto(Character character) {
        this.id = character.getId().toString();
        this.name = character.getCharacterName();
        this.alias = character.getAlias();
        this.gender = character.getGender();
        this.picture = character.getPicture();
    }

    public CharacterDto() {

    }

    // GETTERS AND SETTERS WITH @JsonGetter annotation
    // to parse the variables into the JSON format
    @JsonGetter("id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonGetter("name")
    public String getCharacterName() {
        return name;
    }

    public void setCharacterName(String characterName) {
        this.name = characterName;
    }

    @JsonGetter("alias")
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @JsonGetter("gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonGetter("picture")
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
