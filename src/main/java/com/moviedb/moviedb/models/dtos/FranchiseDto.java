package com.moviedb.moviedb.models.dtos;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.moviedb.moviedb.models.domain.Franchise;

// DTO for Franchise class to encapsulate the information for the controller
public class FranchiseDto {

    private String id;
    private String name;
    private String description;

    // CONSTRUCTORS
    public FranchiseDto(Franchise franchise) {
        this.id = franchise.getId().toString();
        this.name = franchise.getName();
        this.description = franchise.getDescription();
    }

    public FranchiseDto() {

    }


    // GETTERS AND SETTERS WITH @JsonGetter annotation
    // to parse the variables into the JSON format
    @JsonGetter("id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonGetter("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonGetter("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
