package com.moviedb.moviedb.models.dtos;

import java.util.List;

// DTO for a Movie ID list to encapsulate the information for the controller
public class MovieIDsDto {
    List<Long> movieIds;

    // CONSTRUCTOR
    public List<Long> getMovieIds() {
        return movieIds;
    }

    // SETTER
    public void setMovieIds(List<Long> movieIds) {
        this.movieIds = movieIds;
    }
}
