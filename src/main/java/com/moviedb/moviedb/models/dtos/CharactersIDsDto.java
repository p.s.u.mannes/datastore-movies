package com.moviedb.moviedb.models.dtos;

import java.util.List;

// DTO for a list of Character id's to encapsulate the information for the controller
public class CharactersIDsDto {
    List<Long> characterIds;

    // CONSTRUCTOR
    public List<Long> getCharacterIds() {
        return characterIds;
    }

    public void setCharacterIds(List<Long> characterIds) {
        this.characterIds = characterIds;
    }
}
